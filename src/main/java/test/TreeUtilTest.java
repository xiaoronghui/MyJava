package test;

import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.lang.tree.parser.DefaultNodeParser;

public class TreeUtilTest {
	
	public static void main(String[] args) {
		
    	//文件树状节点列表
    	List<TreeNode<Long>> folderTreeNodeList = new ArrayList<TreeNode<Long>>();
    	
    	Long rootNodeId = 1000L;
    	
    	folderTreeNodeList.add(new TreeNode<Long>(1L, 1000L, "水果", 0));
    	folderTreeNodeList.add(new TreeNode<Long>(11L, 1L, "苹果", 0));
    	folderTreeNodeList.add(new TreeNode<Long>(12L, 1L, "苹果", 1));
    	folderTreeNodeList.add(new TreeNode<Long>(13L, 1L, "苹果", 2));
    	
    	folderTreeNodeList.add(new TreeNode<Long>(2L, 1000L, "办公用品", 1));
    	folderTreeNodeList.add(new TreeNode<Long>(21L, 2L, "铅笔", 0));
    	folderTreeNodeList.add(new TreeNode<Long>(22L, 2L, "尺子", 1));
    	folderTreeNodeList.add(new TreeNode<Long>(23L, 2L, "木质用品", 2));
    	folderTreeNodeList.add(new TreeNode<Long>(231L, 23L, "桌子", 0));
    	folderTreeNodeList.add(new TreeNode<Long>(232L, 23L, "床", 0));
		
    	//构建树
    	List<Tree<Long>> treeList = TreeUtil.build(folderTreeNodeList , rootNodeId , new TreeNodeConfig(), new DefaultNodeParser<Long>());
		
    	for(Tree<Long> tree : treeList ) {
    		System.out.println(tree);
    	}
    	
    	
	}
}
