package test;

import java.awt.Font;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileSystemView;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

import cn.hutool.core.io.FileUtil;
import cn.hutool.poi.word.Word07Writer;

public class Test {

	public static void main(String[] args) throws Exception {
		
		String dirPath = "D:/项目示例/gcp-file-material";
		
		String docName = "数垚科技 - 文件物料管理系统源码.doc";
		
		String docTitle = "《数垚科技 - 文件物料管理系统源码说明》";
		
		List<File> fileList = findFileListBySuffix(dirPath, "java");
		
		 // 创建Word07Writer对象，用于生成Word文档
        Word07Writer writer = new Word07Writer();
        
        Font font = new Font("宋体", Font.BOLD, 16);
        
        Font fileTitleFont = new Font("宋体", Font.BOLD, 12);
        
        //内容字体
        Font contentFont = new Font("宋体", Font.PLAIN, 10);
 
        // 添加一个段落
        writer.addText(ParagraphAlignment.CENTER , font , docTitle);
        
        for(int i = 0 ;  i< fileList.size() ; i ++) {
        	
        	File item = fileList.get(i);
        	
        	String fileTitle = (i + 1) + "." + item.getName();
        	
            writer.addText(ParagraphAlignment.LEFT , fileTitleFont , fileTitle);
            
            writer.addText(contentFont, "");
            
        	List<String> lines = FileUtil.readLines(item, Charset.forName("utf-8"));
			
			for(String line : lines) {
				writer.addText(contentFont, line);
			}
			
        }
		
	    writer.flush(new File(getDesktopPath() + "/" + docName));
	    
        // 关闭writer
        writer.close();
        
    }
	
	/**
	 * @function 查找以某个后缀的所有文件
	 * @param dirPath 文件夹路径
	 * @param suffix 后缀名称
	 * @author 肖荣辉
	 * @date 2024年11月28日
	*/
	public  static List<File>  findFileListBySuffix(String dirPath , String suffix){
		
		List<File> fileList = new ArrayList<File>();
		
		List<File> list = FileUtil.loopFiles(dirPath);
		
		suffix = suffix.startsWith(".") ? suffix : "." + suffix;
		
		for(File file : list) {
			
			if(file.isDirectory()) continue;
			
			if(file.getName().endsWith(suffix))
				fileList.add(file);
			
		}
		
		return fileList;
	}
	
	/**
	 *  获取桌面路径
	 */
	public static String getDesktopPath(){
		return FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath();
	}
		
}
