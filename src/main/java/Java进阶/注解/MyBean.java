package Java进阶.注解;

import java.lang.reflect.Field;

import lombok.Data;

@Data
public class MyBean {
	
	@LoginAnnotation("哈哈哈")
	private String name;
	
	
	@LoginAnnotation( value = "呀呀呀")
	private String color;
	
	
	public MyBean() {
		super();
	}

	public MyBean(String name, String color) {
		super();
		this.name = name;
		this.color = color;
	}


	 public static void main(String[] args) throws NoSuchFieldException, SecurityException {
		 
		 Class<?> clazz = MyBean.class;
		 
		 Field field1 = clazz.getDeclaredField("name");
		 Field field2 = clazz.getDeclaredField("color");
		 
		 System.out.println(field1.getName());

		 LoginAnnotation a1 = field1.getAnnotation(LoginAnnotation.class);
		 LoginAnnotation a2 = field2.getAnnotation(LoginAnnotation.class);
		 
		 //省略属性名,值就是value
		 System.out.println(a1.value());
		 System.out.println(a2.value());
		 
		 //不填写值,取默认值
		 System.out.println(a2.color());
		 
		 
	}
		
	
}
