package Java进阶.注解;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  注解内的都是属性,使用如下
 * @LoginAnnotation(value="aaaa")
 * 
 * 由于value属性没有写default,所以直接使用@LoginAnnotatio()是会报错的,
 * 
 * 当不写属性名的时候 , 默认是赋值给value属性的
 * 
 * @LoginAnnotation("aaaaa") , 比如
 * 
 */
@Retention(RetentionPolicy.RUNTIME)  //要保留到运行期间
@Target({ElementType.FIELD})  //标识用在属性上
public @interface LoginAnnotation {
	
	//String 类型的属性,属性名为color
	String color() default "blue";  //默认为蓝色,若是不填默认值,则该属性必填
	
	//可以作为默认值的属性
	String value();

}
