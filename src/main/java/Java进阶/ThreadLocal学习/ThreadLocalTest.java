package Java进阶.ThreadLocal学习;

import java.util.Random;
import java.util.stream.IntStream;

public class ThreadLocalTest {
	
	public static void main(String[] args) {
		
		ThreadLocal<String> local = new ThreadLocal<String>();
		
		Random random = new Random();
		
		//创建5个线程
		IntStream.range(0, 5).forEach(a -> new Thread(()->{
//			local.set( a  + " :  " + random.nextInt(10000));
			System.out.println(local.get());
		}).start()) ;
		
		//其实就是把当前线程作为key,把要存入的信息到map的一个对象 , 用于当前线程共享.
		
	}

}
