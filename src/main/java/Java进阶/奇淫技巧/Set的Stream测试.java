package Java进阶.奇淫技巧;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *  真香 , 真香,  真香 !!!
 */
public class Set的Stream测试 {
	
	public static void main(String[] args) {
		
		//参考 : https://blog.csdn.net/qq_40301026/article/details/108515113
		
		//参考 
		Set<Integer> set = new HashSet<Integer>();
		set.addAll(Arrays.asList(1, 2 , 2,24,756,23,54,46,24356,456,21));
		
		/**
		 * stream并不是某种数据结构，它只是数据源的一种视图。这里的数据源可以是一个数组，
		 * Java容器或I/O channel等。正因如此要得到一个stream通常不会手动创建，而是调用对应的工具方法，比如：
		   调用Collection.stream()或者Collection.parallelStream()方法
		   调用Arrays.stream(T[] array)方法
		 * 
		 * filter方法 : 
		 *  调用filter()Stream流中元素就剩下了符合要求的元素，可以继续对这些元素进行操作
		 */
		Stream<Integer> s = 	set.stream();
		s  = s.filter(x -> x < 100);  //获取小于100的那些元素 , 又返回一个stream
		
		//去重
		s = s.distinct();
		
		//对每个元素进行某种操作 , 比如我进行打印
		//s.forEach(System.out::println);
		
		//看看是否都小于100
//		s.forEach(x -> {
//			System.out.println(  x < 100);
//		});
		
		//变回一个List
//		ArrayList<Integer> arrayList = s.collect(Collectors.toCollection(ArrayList::new));
//		System.out.println(arrayList);
		
		//默认正序 , 用过一次不能继续用了
//		Iterator<Integer> iterator = s.sorted().iterator();
//		System.out.println(JSON.toJSONString(iterator));
		
		/**
		 * 作用是返回一个对当前所有元素执行mapper之后的结果组成的Stream。直观的说，就是对每个元素按照某种操作进行转换，
		 * 转换前后Stream中元素的个数不会改变，但元素的类型取决于转换之后的类型。
		 *  其实就是对元素进行你想要的转换 , 比如下面的的给每个元素加一个前缀
		 */
		Stream<Integer> stream = Stream.of(1, 2 , 2,24,756,23,54,46,24356,456,21);
		stream.map(x -> "A" + x).forEach(str -> System.out.println(str));
		    
		
	}

}
