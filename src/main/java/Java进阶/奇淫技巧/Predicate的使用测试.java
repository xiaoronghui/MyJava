package Java进阶.奇淫技巧;

import java.util.function.Predicate;

public class Predicate的使用测试 {
	
	
	public static void main(String[] args) {
		
		//目的是构造一个test判断表达式,判断逻辑随自己编写
		Predicate<Integer> predicate = new Predicate<Integer>() {
            @Override
            public boolean test(Integer s) {
                return s > 3;
            }
        };
        
        Predicate<Integer> predicate1 = new Predicate<Integer>() {
            @Override
            public boolean test(Integer s) {
                return s > 3;
            }
        };
        
        Predicate<Integer> predicate2 = x -> x < 100 ;
        
        /**
         * and方法的使用
         * 和别的predicate进行逻辑与,相当于&& , 用来进行多个判断
         * 下面的判断逻辑是 == >   3<x<100
         */
        predicate = predicate.and(predicate2);
        
        System.out.println(predicate.test(3));
        System.out.println(predicate.test(100));
        System.out.println(predicate.test(99));
        
        /**
         * negate方法的使用
         * 返回一个和原test方法相反判断的predicate实例
         */
        System.out.println("negate方法的使用==>返回一个和原test方法相反判断的predicate实例==");
        predicate =  predicate.negate();
        System.out.println(predicate.test(3));
        System.out.println(predicate.test(100));
        System.out.println(predicate.test(99));
        
        /**
         * or方法的使用
         * 和别的predicate进行逻辑或,相当于|| , 用来进行多个判断
         * 下面的判断逻辑是 == >   3<x  || x<-2
         */
        
        System.out.println("==or方法的使用==");
        predicate = predicate1.or(x -> x < -2);
        System.out.println(predicate.test(4));
        System.out.println(predicate.test(3));
        System.out.println(predicate.test(-5));
        
        
	}
	
	

}
