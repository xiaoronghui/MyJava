package Java进阶.NIO设计的初衷;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MyClient2 {
	
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		
		Socket socket = new Socket("127.0.0.1", 8888);
		
		//故意不发送消息,那么服务端就会一直阻塞在read方法,别的客户端就连接不上
		Scanner scanner = new Scanner(System.in);
		
		socket.getOutputStream().write(scanner.next().getBytes());
		socket.close();
		
	}
	
}
