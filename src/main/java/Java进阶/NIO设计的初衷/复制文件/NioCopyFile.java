package Java进阶.NIO设计的初衷.复制文件;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class NioCopyFile {
	
	
	public static void main(String[] args) throws IOException {
		
		FileChannel inChannel = FileChannel.open(Paths.get("D:/药品JSON.txt"), StandardOpenOption.READ);
        FileChannel outChannel = FileChannel.open(Paths.get("D:/文件复制.txt") , StandardOpenOption.WRITE , StandardOpenOption.CREATE);
      
        //从inChannel通道获取
        outChannel.transferFrom(inChannel,0, inChannel.size()); 
        inChannel.close();
        outChannel.close();
		
	}
	
	
}
