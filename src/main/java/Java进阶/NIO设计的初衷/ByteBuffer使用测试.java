package Java进阶.NIO设计的初衷;

import java.io.ByteArrayInputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;

public class ByteBuffer使用测试 {
	
	
	
	public static void main(String[] args) {
		
		
		/**
		 * 
		 * 属性 :
		 * 
		 *  byte[] buff  //buff即内部用于缓存的数组。
		     position //当前读取的位置。
		     mark //为某一读过的位置做标记，便于某些时候回退到该位置。
		     capacity //初始化时候的容量。
		     limit //当写数据到buffer中时，limit一般和capacity相等，当读数据时，limit代表buffer中有效数据的长度。
		 * 
		 * 常规方法:
		 * 
		 *  ByteBuffer allocate(int capacity) //创建一个指定capacity的ByteBuffer。
		     ByteBuffer allocateDirect(int capacity) //创建一个direct的ByteBuffer，这样的ByteBuffer在参与IO操作时性能会更好
		     ByteBuffer wrap(byte [] array)
		     ByteBuffer wrap(byte [] array, int offset, int length) //把一个byte数组或byte数组的一部分包装成ByteBuffer。
		     //把源数组放到byteBuffer中
		    put(byte[] src) 
		     byte get(int index)
		     ByteBuffer put(byte b)
		     int getInt()       　　　　　　//从ByteBuffer中读出一个int值。
		     ByteBuffer putInt(int value)  // 写入一个int值到ByteBuffer中。
		 * 
		 * 特殊方法 :
		 * 
		 *   Buffer clear()    把position设为0，把limit设为capacity，一般在把数据写入Buffer前调用。
		     Buffer flip() 　  把limit设为当前position，把position设为0，一般在从Buffer读出数据前调用。
		     Buffer rewind()  把position设为0，limit不变，一般在把数据重写入Buffer前调用。
		     compact()       将 position 与 limit之间的数据复制到buffer的开始位置，复制后 position = limit -position,limit = capacity, 
		     但如果position 与limit 之间没有数据的话发，就不会进行复制。
		     mark() & reset()     通过调用Buffer.mark()方法，可以标记Buffer中的一个特定position。之后可以通过调用Buffer.reset()方法恢复到这个position。
		 */
		
		String str = "你好呀";
		byte[] buff = str.getBytes();
		
		System.out.println("字符串字节数:" + buff.length);
		
		//若buff不够用,则会报错
		ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
		
		byteBuffer.put(buff);
		
		System.out.println("position : " + byteBuffer.position()  + " limit : " + byteBuffer.limit()  );
		
		
		byteBuffer.flip();
		System.out.println("调用flip之后 ==> position : " + byteBuffer.position()  + " limit : " + byteBuffer.limit()  );
		
		//clear之后,m会设置为-1,所以调用reset会报异常
		byteBuffer.clear();
		System.out.println("调用clear之后 ==> position : " + byteBuffer.position()  + " limit : " + byteBuffer.limit()  );
		
		//标记这个位置
		byteBuffer.position(5);
		Buffer bu = byteBuffer.mark();
		System.out.println(bu.position());
		
		//获取一个字节,position会往前+1
		byteBuffer.get();
		
		byteBuffer.reset();
		System.out.println("调用reset之后 ==> position : " + byteBuffer.position()  + " limit : " + byteBuffer.limit()  );
		
	}
	
	

}
