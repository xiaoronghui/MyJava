package Java进阶.NIO设计的初衷;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MyClient {
	
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		
		Socket socket = new Socket("127.0.0.1", 8888);
		
		//故意不发送消息,那么服务端就会一直阻塞在read方法,别的客户端就连接不上
		//这个时候我们用网络调试助手来连接服务器,发现根本就连接不上
		Scanner scanner = new Scanner(System.in);
		
		socket.getOutputStream().write(scanner.next().getBytes());
		socket.close();
		
	}
	
}
