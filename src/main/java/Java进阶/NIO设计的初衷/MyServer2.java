package Java进阶.NIO设计的初衷;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer2 {
	
	
	/**
	 * 传统的阻塞式服务器
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		ServerSocket serverSocket = new ServerSocket(8888);
		
		System.out.println("server start ,  listen port 8888");
		
		while(true) {
			
			System.out.println("wait connect..");
			
			//这里是阻塞的,只要没有客户端连接,就无法往下执行
			Socket socket = serverSocket.accept();
			
			//每次来个连接都开启一个线程
			new Thread(new  Runnable() {
				
				@Override
				public void run() {
					
					try {
						
						System.out.println("connect success");
						
						byte[] buff = new byte[1024];
						
						//等待客户端发送消息,客户端不发送,则无法往下执行
						socket.getInputStream().read(buff);
						
						System.out.println("read success");
						
						System.out.println(new String(buff));
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
			}).start();
			
			
			
		}
		
	}
	

}
