package Java进阶.Class类的方法测试;


//所以我们可以判断是否是成员类来判断一个类是不是一个内部类
//参考 : https://www.zhihu.com/question/67270393
public class ClassTest {
	
	  public ClassTest() {
		  print("当前类" ,  getClass());
	}

	static class StaticMemberClass {
	        int a = print("静态成员类" ,  getClass());
	    }

	    public class MemberClass {
	        int a = print("成员类" , getClass());
	    }

	    public static void main(String[] args) {

	        // 匿名类
	        new Cloneable() {
	            int a = print("匿名内部类 : " ,  getClass());
	        };

	        // 成员类
	        new ClassTest().new MemberClass();

	        // 静态成员类
	        new StaticMemberClass();

	        // 局部类
	        class LocalClass {
	            int a = print("局部类" , getClass());
	        }

	        new LocalClass();
	        
	        new ClassTest();

	    }

	    private static int print(String title , Class<?> clazz) {
	    	 System.out.println("-------------"+title+"----------------");
	        System.out.println(String.format("%s isMemberClass : %s", clazz.getName(), clazz.isMemberClass()));
	        System.out.println(String.format("%s isLocalClass : %s", clazz.getName(), clazz.isLocalClass()));
	        System.out.println(String.format("%s isAnonymousClass : %s", clazz.getName(), clazz.isAnonymousClass()));
	        return 0; // 返回值没有用，就为了在成员创建的时候可以调用此函数
	    }

}
