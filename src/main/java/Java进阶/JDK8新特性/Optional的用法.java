package Java进阶.JDK8新特性;

import java.util.ArrayList;
import java.util.Optional;

import common.testbeans.Dog;

/**
 * 
 * Option类是判断空指针异常时用的，对于其他的if..else只要不是判断空指针的问题，
 *	就不要使用Option类，lz就陷入了一个误区，以为学了Option类，看见if...else就往Option来想，
 *  这么做真是大作特错。用Option的情况时在判断值是否为null时，Option就发挥作用了。
*/
public class Optional的用法 {

	public static void main(String[] args) throws Exception {
		
//		1. 创建：
//
//	    Optional.empty()： 创建一个空的 Optional 实例
		
		Optional<Object> p1 = Optional.empty();
		
		System.out.println(p1.isPresent());
		
//
//	    Optional.of(T t)：创建一个 Optional 实例，当 t为null时抛出异常      
		
		Optional<Object> p2 = Optional.of(new Object());
		
		System.out.println(p2.isPresent());
//
//	    Optional.ofNullable(T t)：创建一个 Optional 实例，但当 t为null时不会抛出异常，而是返回一个空的实例
		
		Optional<Object> p3 = Optional.ofNullable(null);

		System.out.println(p3.isPresent());
		
//		2. 获取：
//
//	    get()：获取optional实例中的对象，当optional 容器为空时报错
	
//		3. 判断：
//
//	    isPresent()：判断optional是否为空，如果空则返回false，否则返回true
		
//	    ifPresent(Consumer c)：如果optional不为空，则将optional中的对象传给Comsumer函数
		
		p3.ifPresent(item->System.out.println(item));
		
//	    orElse(T other)：如果optional不为空，则返回optional中的对象；如果为null，则返回 other 这个默认值
		
		System.out.println(p3.orElse(3));
//
//	    orElseGet(Supplier<T> other)：如果optional不为空，则返回optional中的对象；如果为null，则使用Supplier函数生成默认值other
		
		//若为空 , 返回一个空数组
		System.out.println(p3.orElseGet(ArrayList::new));
		
//
//	    orElseThrow(Supplier<X> exception)：如果optional不为空，则返回optional中的对象；如果为null，则抛出Supplier函数生成的异常
		
		//p3.orElseThrow(()->new Exception());
		
//		4. 过滤：
//
//	    filter(Predicate<T> p)：如果optional不为空，则执行断言函数p，如果p的结果为true，则返回原本的optional，否则返回空的optional  
		p3.filter(item->"abc".equals(item));
		
//		5. 映射：
//
//	    map(Function<T, U> mapper)：如果optional不为空，则将optional中的对象 t 映射成另外一个对象 u，并将 u 存放到一个新的optional容器中。
//
//	    flatMap(Function< T,Optional<U>> mapper)：跟上面一样，在optional不为空的情况下，将对象t映射成另外一个optional
//
//	    区别：map会自动将u放到optional中，而flatMap则需要手动给u创建一个optional
		
		Dog dog1 = new Dog(" 小黄 " , 1);
		
		Optional<Dog> p4 = Optional.of(dog1);
		
		//获取对象的名称同时去空格
		System.out.println(p4.map(Dog::getName).map(String::trim).filter(item->item != null).get());
		
		//flatMap参考 : https://blog.csdn.net/qq_35634181/article/details/101109300
		
		/*
		 * 如果某对象实例的属性本身就为Optional包装过的类型，那么就要使用flatMap方法，
		 * 就像School::getTearch返回的就是Optional<Tearch>类型的，所以不用再使用Optional进行包装，
		 * 这时就要选用flatMap方法，对于返回值是其他类型，需要Optional进行包装，如Student::getName得到是String类型的，
		 * 就需要使用map方法在其外面包装一层Optional对象。
		 */
		
		
	}
	
}
