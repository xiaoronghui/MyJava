package Java进阶.JDK8新特性;

//注解这是一个函数式接口
@FunctionalInterface
public interface DefaultAndStaticMethodInf {
	
	//接口中允许定义静态方法
	static void fun1() {
		System.out.println(1);
	}
	
	//接口中允许默认方法
	default void fun2() {
		System.out.println(2);
	}
	
	//接口中允许默认方法
	default void fun3() {
			System.out.println(2);
	}
	
	//声明一个抽象方法
	void fun4();
	
	//因为接口中可以定义静态方法,所以我们可以在接口中写main方法来测试了
	public static void main(String[] args) {
		DefaultAndStaticMethodInf.fun1();
	}
	
}
