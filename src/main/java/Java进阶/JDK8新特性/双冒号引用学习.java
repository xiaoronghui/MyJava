package Java进阶.JDK8新特性;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class 双冒号引用学习 {

	public static void  printValur(String str){
        System.out.println("print value : "+str);
    }
 
    public static void main(String[] args) {
 
        System.out.println("----------------普通的写法-----------------------");
        List<String> al = Arrays.asList("a","b","c","d");
        for (String a: al) {
        	双冒号引用学习.printValur(a);
        }
        System.out.println("----------------JDK双冒号--------------------------");
        
        //双冒号得到的是Comsumer,所有用@FunctionalInterface注解的都可以用lambda表示.
       
        //JDK8中有双冒号的用法，就是把方法当做参数传到stream内部，使stream的每个元素都传入到该方法里面执行一下
        Consumer<String> stringConsumer = 双冒号引用学习::printValur;
        
        System.out.println("----------------方式1--------------------------");
        
        al.forEach(stringConsumer);
        System.out.println("----------------方式2--------------------------");

        al.forEach(x -> stringConsumer.accept(x));
        System.out.println("----------------方式3--------------------------");
        
        al.forEach(双冒号引用学习::printValur);
    }

	
}
