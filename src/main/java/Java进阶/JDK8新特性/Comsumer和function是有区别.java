package Java进阶.JDK8新特性;

import java.util.function.Consumer;
import java.util.function.Function;

import Java基础.模拟ArrayList.Cat;

public class Comsumer和function是有区别 {

	
	public static void main(String[] args) {
		
        //Comsumer和function是有区别, comsumer只执行, 不返回值,称之为消费者也有道理,而funciton是有返回值的, 这就是他们的区别
        //他们都可以代表函数,举例
		
		Cat cat = new Cat("小花猫", 11);
        
		//他们两都可以接受双冒号返回的函数 , 只是一个可以定义返回值,另外一个不可以
        Function<Cat , Integer> a = Cat::getAge;
        
        Consumer<Cat> b = Cat::getAge;
        
        System.out.println(a.apply(cat));
        
        //返回的是void
        b.accept(cat);
        
        
        //还有Predicate,IntFunction,DoubleFunction , 相对于Funciton来说,只是确认了返回值而且,不需要去写返回值了
	}
	
}
