package lombok常用注解;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.extern.java.Log;

@Data
@EqualsAndHashCode(callSuper=false)
@Log
@SuperBuilder
@Accessors(chain = true)
public class User extends BaseUser { 

	private final int a = 1;
	@NonNull private Integer id;
	private String name;
	private double money;
	private Date birthday;
	
	
	public static void main(String[] args) {
		
		log.info("在马勒戈壁的荒漠上...");
		
		//@Builder注解可以达到链式操作,但是不支持父类的属性操作,我们可以使用@SuperBuilder注解来达到
		User user = User.builder().name("马超").id(1).color("你好呀").build();
		
		//@Accessors(chain = true) 会使set方法返回当前对象
		user.setName("小明");
		
		//canEqual方法可以用来判断instanceof
		System.out.println(user.canEqual(new Object()));
		
		System.out.println(user);
		
		
	}
	
	
}
