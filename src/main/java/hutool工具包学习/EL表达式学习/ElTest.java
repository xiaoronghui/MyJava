package hutool工具包学习.EL表达式学习;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.extra.expression.ExpressionUtil;

public class ElTest {
	
	public static void main(String[] args) {
		
		String elExpression = "{userName==projectName}";
		
		Map<String , Object> map = new HashMap<String , Object>();
		
		map.put("userName", "驳回");
		map.put("projectName", "驳回1");
		
		Object[] obj = (Object[]) ExpressionUtil.eval(elExpression, map);
		
		for(Object object : obj) {
			
			System.out.println(object);
		}
		
	}
	
}
