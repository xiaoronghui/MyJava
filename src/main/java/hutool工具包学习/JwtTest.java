package hutool工具包学习;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;

/**
 * @function JWT登录工具类测试
 * @author 肖荣辉
 * @date 2022年9月5日
*/
public class JwtTest {

	public static void main(String[] args) throws InterruptedException {
		
		//多少秒后过期
		int expireSecond = 10;
		
		DateTime now = DateTime.now();
		
		//过期时间
		DateTime expireDate = now.offsetNew(DateField.SECOND, expireSecond);
		
		//载和信息
		Map<String , Object> payload = new HashMap<String, Object>();
		
		//签发时间
		payload.put(JWTPayload.ISSUED_AT , now);
		
		//过期时间
		payload.put(JWTPayload.EXPIRES_AT , expireDate);
		
		//生效时间
		payload.put(JWTPayload.NOT_BEFORE , now);
		
		//主题
		payload.put(JWTPayload.SUBJECT , "ServerScanLoingUserInfo");
		
		//存入数据
		payload.put("account", "admin");
		
		payload.put("password", "shuyaokeji@good666");
		
		String key = "shuyaokeji";
		
		String token = JWTUtil.createToken(payload, key.getBytes());
		
		System.out.println(token);
		
		//解析token
		JWT jwt = JWTUtil.parseToken(token);
		
		//获取数据
		JSONObject payloads = jwt.getPayloads();
		
		System.out.println("载荷数据 : " + JSONUtil.toJsonPrettyStr(payloads));
		
		//验证token是否合法有效
		boolean flag = jwt.setKey(key.getBytes()).verify();
		
		System.out.println("token是否正确 : " + flag);

		//验证token的有效期是否有效
		boolean flag2 = jwt.validate(0);

		System.out.println("验证有效期是否正确 : " + flag2);
		
		//休眠10秒
		Thread.sleep(10000);
		
		System.out.println(expireSecond + "秒后 , token是否正确 : " +  jwt.setKey(key.getBytes()).verify());
		System.out.println(expireSecond + "秒后 , 验证有效期是否正确 : " +  jwt.validate(0));
	}
	
	
	
}
