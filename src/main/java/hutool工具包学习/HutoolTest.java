package hutool工具包学习;

import cn.hutool.core.util.DesensitizedUtil;
import cn.hutool.core.util.StrUtil;

public class HutoolTest {
	
	//参考https://blog.csdn.net/qq_42981242/article/details/110939210
	public static void main(String[] args) {
		
		
		//字符串校验
		//StrUtil.isBlank(str)
		
		//json相关
//		JSONUtil.toJsonStr("");
//		JSONUtil.parseObj("xxx");
//		JSONUtil.toBean(json, beanClass);
//		JSONUtil.toList(jsonArray, elementType)
		
		//远程http调用相关
//		HttpUtil.post(urlString, paramMap)
		
		System.out.println(StrUtil.desensitized("段正淳", DesensitizedUtil.DesensitizedType.CHINESE_NAME));
	}
	

}
