package common.testbeans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dog {
	
	//姓名
	private String name;
	
	//年龄
	private int age;
	
}
