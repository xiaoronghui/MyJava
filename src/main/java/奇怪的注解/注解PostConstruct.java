package 奇怪的注解;

import javax.annotation.PostConstruct;

public class 注解PostConstruct {
	
	
	static class Cup{
		
		@PostConstruct
		public void say() {
			System.out.println("say 方法被调用");
		}
		
		public Cup() {
			System.out.println("Cup 的构造方法");
		}
		
	}
	
	public static void main(String[] args) {
		
		//参考 : https://www.jianshu.com/p/98cf7d8b9ec3
		
		/**
		 * 
		 * 
		 * @PostConstruct和@PreDestroy，这两个注解被用来修饰一个非静态的void（）方法
		 * 
		 * 被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器执行一次。
		 * PostConstruct在构造函数之后执行，init（）方法之前执行。PreDestroy（）方法在destroy（）方法知性之后执行
		 * 
		 * 另外，spring中Constructor、@Autowired、@PostConstruct的顺序
			其实从依赖注入的字面意思就可以知道，要将对象p注入到对象a，那么首先就必须得生成对象a和对象p，
			才能执行注入。所以，如果一个类A中有个成员变量p被@Autowried注解，那么@Autowired注入是发生在A的构造方法执行完之后的。
			如果想在生成对象时完成某些初始化操作，而偏偏这些初始化操作又依赖于依赖注入，那么久无法在构造函数中实现。
			为此，可以使用@PostConstruct注解一个方法来完成初始化，@PostConstruct注解的方法将会在依赖注入完成后被自动调用。
			
			好处:
			
				1.根据注解的顺序,依赖的注入发生在构造方法之后, 假设我们就是想在构造方法中使用依赖的对象,
				  这个时候会空指针,而使用@PostConstruct确可以达到这样的效果
			
			   2.我们不想修改构造方法,又想新增逻辑的时候,可以使用这种方式
		 * 
		 */
		
		//不是spring好像是没有效果
		
		/**
		 * 只有在spring启动的时候,初始化这个bean的时候,才会有效果,直接在这里使用没有效果
		 */
		Cup cup = new Cup();
		
	}
	
	

}
