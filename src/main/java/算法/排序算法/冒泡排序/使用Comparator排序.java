package 算法.排序算法.冒泡排序;

import java.util.Comparator;

public class 使用Comparator排序 {
	
	class MyCompara implements Comparator<Integer> {
		
	    public int compare(Integer o1, Integer o2) {
	       //倒序前一个小于后一个返回正值就可实现，如果是要正序就是前一个大于后一个就返回正值
	        if (o1 < o2) {
	            return 1;
	        }
	        else if (o1 > o2) {
	            return -1;
	        }
	        else {
	            return 0;
	        }
	    }
	 
	}

}
