package Java基础.字符串;

public class format的使用 {
	
	public static void main(String[] args) {
	
//		转换符	详细说明	示例
//		%s	字符串类型	“你好啊伙计”
//		%c	字符类型	‘m’
//		%b	布尔类型	true
//		%d	整数类型	（十进制） 88
//		%x	整数类型	（十六进制） FF
//		%o	整数类型	（八进制） 77
//		%f	浮点类型	8.888
//		%a	十六进制浮点类型	FF.35AE
//		%e	指数类型	9.38e+5
//		%g	通用浮点类型（f和e类型中较短的）	不举例(基本用不到)
//		%h	散列码	不举例(基本用不到)
//		%%	百分比类型	％(%特殊字符%%才能显示%)
//		%n	换行符	不举例(基本用不到)
//		%tx	日期与时间类型（x代表不同的日期与时间转换符)	不举例(基本用不到)
//     %02x  表示以16进制输出,不足2位前面补0 , 不足多少位可以补
		
	   String str=null;  
	    str=String.format("Hi,%s", "小明");  
	    System.out.println(str);  //Hi,小明
	    str=String.format("Hi,%s %s %s", "小明","是个","大帅哥");            
	    System.out.println(str); //Hi,小明 是个 大帅哥               
	    System.out.printf("字母c的大写是：%c %n", 'C');  //字母c的大写是：C   
	    System.out.printf("布尔结果是：%b %n", "小明".equals("帅哥"));  //布尔的结果是：false
	    System.out.printf("100的一半是：%d %n", 100/2);  //100的一半是：50
	    System.out.printf("100的16进制数是：%x %n", 100);  // 100的16进制数是：64
	    System.out.printf("100的8进制数是：%o %n", 100);  //100的8进制数是：144
	    System.out.printf("50元的书打8.5折扣是：%f 元%n", 50*0.85);  //50元的书打8.5折扣是：42.500000 元  
	    System.out.printf("上面价格的16进制数是：%a %n", 50*0.85);  //上面价格的16进制数是：0x1.54p5   
	    System.out.printf("上面价格的指数表示：%e %n", 50*0.85);  //上面价格的指数表示：4.250000e+01   
	    System.out.printf("上面价格的指数和浮点数结果的长度较短的是：%g %n", 50*0.85);  //上面价格的指数和浮点数结果的长度较短的是：42.5000   
	    System.out.printf("上面的折扣是%d%% %n", 85);  //上面的折扣是85%   
	    System.out.printf("字母A的散列码是：%h %n", 'A');//字母A的散列码是：41 
		
	    //以16进制输出,不足8位前面补0
		System.out.println(String.format("%08x", 34534));
		
	}

}
