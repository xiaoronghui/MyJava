package Java并发包.同一个用户操作同步;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import cn.hutool.core.util.RandomUtil;

public class ReentrantLockTest {
	
	private final ConcurrentHashMap<String, Lock> userLocks = new ConcurrentHashMap<>();

	public static void main(String[] args) {
		
		ReentrantLockTest test  = new ReentrantLockTest();
		
		//对的 , 符合预期
//		test.testSameUserId();
		
		//对的符合预期
		test.testDiffUserId();
	}
	
	
	/**
	 * @function 测试不同用户
	 * @author 肖荣辉
	 * @date 2024年12月9日
	*/
	public void testDiffUserId() {
		
		for(int i = 0 ;i < 5 ;i ++) {
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					myMethod(RandomUtil.randomString(5));
				}
			}).start();
		}
		
	}
	
	
	/**
	 * @function 测试同一个用户
	 * @author 肖荣辉
	 * @date 2024年12月9日
	*/
	public void testSameUserId() {
		
		String userId = "423432";
		
		for(int i = 0 ;i < 5 ;i ++) {
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					myMethod(userId);
				}
			}).start();
		}
		
	}
	
	
	/**
	 * @function 同一个userId操作myMethod的时候要相互等待,不同用户没有关系
	 * @author 肖荣辉
	 * @date 2024年12月9日
	*/
	public void myMethod(String userId) {
		
		Lock lock = userLocks.computeIfAbsent(userId , k -> new ReentrantLock());
		
		lock.lock();
		
		try {
			
			Thread.sleep(1000);
			
			System.out.println(Thread.currentThread().getName() + "==>" + userId);
			
		}catch (Exception e) {
			lock.unlock();
		} finally {
			lock.unlock();
		}
		
	}
	
	
}
