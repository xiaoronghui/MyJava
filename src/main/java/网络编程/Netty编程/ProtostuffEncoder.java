package 网络编程.Netty编程;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.protostuff.LinkedBuffer;
import io.protostuff.ProtobufIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;

//用于将Person对象编码成字节数组
public class ProtostuffEncoder extends MessageToByteEncoder<MyNettyMsg> {

  @Override
  protected void encode(ChannelHandlerContext ctx, MyNettyMsg msg, ByteBuf out) throws Exception {
      LinkedBuffer buffer = LinkedBuffer.allocate(1024);
      Schema<MyNettyMsg> schema = RuntimeSchema.getSchema(MyNettyMsg.class);
      byte[] array = ProtobufIOUtil.toByteArray(msg, schema, buffer);
      out.writeBytes(array);
  }

}