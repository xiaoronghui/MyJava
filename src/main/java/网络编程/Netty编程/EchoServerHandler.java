package 网络编程.Netty编程;

import java.util.Date;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class EchoServerHandler extends SimpleChannelInboundHandler<MyNettyMsg> {
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		System.out.println("server 读取数据完毕..");
        ctx.flush();//刷新后才将数据发出到SocketChannel
	}
 
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		cause.printStackTrace();
		ctx.close();
	}


	@Override
	protected void messageReceived(ChannelHandlerContext ctx, MyNettyMsg msg) throws Exception {
		System.out.println("服务器接收到数据 : " + msg.getContent());
		
		MyNettyMsg wMsg = new MyNettyMsg("你好呀 , 服务器现在时间是 : " + new Date().toString());
		
		ctx.writeAndFlush(wMsg);
		System.out.println("服务器回写数据 : " + wMsg.getContent());
		
	}
 
}
