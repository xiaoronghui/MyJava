package 网络编程.Netty编程;

import java.util.concurrent.TimeUnit;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.timeout.IdleStateHandler;

public class EchoClientChannelInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		
		System.out.println("调用initChannel");
		
		ChannelPipeline channelPipeline = ch.pipeline();
		//5触发一次读空闲事件,写空闲因为1秒触发一次,所以写空闲触发了5次
		
		channelPipeline.addLast(new IdleStateHandler(5, 1, 0, TimeUnit.SECONDS));
		
		channelPipeline.addLast(new LengthFieldBasedFrameDecoder(10240, 0, 4, 0, 4));
		channelPipeline.addLast(new ProtostuffDecoder());
		channelPipeline.addLast( new LengthFieldPrepender(4));
		channelPipeline.addLast(new ProtostuffEncoder());
		
		channelPipeline.addLast(new HeartBeatServerHandler());//注册handler

	}

}
