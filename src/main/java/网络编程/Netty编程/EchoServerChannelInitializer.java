package 网络编程.Netty编程;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

public class EchoServerChannelInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		
		System.out.println("服务器initChannel");
		
		ChannelPipeline channelPipeline = ch.pipeline();
		
		channelPipeline.addLast(new LengthFieldBasedFrameDecoder(10240, 0, 4, 0, 4));
		channelPipeline.addLast(new ProtostuffDecoder());
		channelPipeline.addLast( new LengthFieldPrepender(4));
		channelPipeline.addLast(new ProtostuffEncoder());
		channelPipeline.addLast(new EchoServerHandler());//注册handler

	}

}
