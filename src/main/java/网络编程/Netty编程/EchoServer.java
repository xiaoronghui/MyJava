package 网络编程.Netty编程;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * • 配置服务器功能，如线程、端口 • 实现服务器处理程序，它包含业务逻辑，决定当有一个请求连接或接收数据时该做什么
 * 
 * @author wilson
 *
 */
public class EchoServer {
 
	private final int port;
 
	public EchoServer(int port) {
		this.port = port;
	}
 
	public void start() throws Exception {
		
		EventLoopGroup eventLoopGroup = null;
		
		try {
			
			//server端引导类
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			
			//连接池处理数据
			eventLoopGroup = new NioEventLoopGroup();
			
			//装配bootstrap
			serverBootstrap.group(eventLoopGroup)
			.channel(NioServerSocketChannel.class)//指定通道类型为NioServerSocketChannel，一种异步模式，OIO阻塞模式为OioServerSocketChannel
			.localAddress("127.0.0.1",port)//设置InetSocketAddress让服务器监听某个端口已等待客户端连接。
			.childHandler(new EchoServerChannelInitializer());
			
			// 最后绑定服务器等待直到绑定完成，调用sync()方法会阻塞直到服务器完成绑定,然后服务器等待通道关闭，因为使用sync()，所以关闭操作也会被阻塞。
			ChannelFuture channelFuture = serverBootstrap.bind().sync();
			System.out.println("开始监听，端口为：" + channelFuture.channel().localAddress());
			
			//下面这句话什么意思? https://blog.csdn.net/yzh_1346983557/article/details/86554837
			
			/**
			 *绑定端口，开启监听是通过异步开启一个子线程执行的 
			 * 当前线程不会同步等待；closeFuture().sync()就是让当前线程(即主线程)同步等待Netty server的close事件，
			 * Netty server的channel close后，主线程才会继续往下执行。closeFuture()在channel close的时候会通知当前线程。
			 * 
			 *  服务端管道关闭的监听器并同步阻塞,直到server channel关闭,线程才会往下执行,结束进程；
		         主线程执行到这里就 wait 子线程结束，子线程才是真正监听和接受请求的，子线程就是Netty启动的监听端口的线程；
		         即closeFuture()是开启了一个子线程server channel的监听器，负责监听channel是否关闭的状态，sync()让主线程同步等待子线程结果
		         
		         若是把sync()去掉,则程序一会会自动关闭
		         
		         客户端的同理
			 * 
			 */
			channelFuture.channel().closeFuture().sync();
		} finally {
			eventLoopGroup.shutdownGracefully().sync();
		}
	}
 
	public static void main(String[] args) throws Exception {
		new EchoServer(20000).start();
	}
}
