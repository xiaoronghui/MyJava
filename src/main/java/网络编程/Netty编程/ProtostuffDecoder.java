package 网络编程.Netty编程;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.protostuff.ProtobufIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;

public class ProtostuffDecoder extends ByteToMessageDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		
		  Schema<MyNettyMsg> schema = RuntimeSchema.getSchema(MyNettyMsg.class); 
		  MyNettyMsg msg =schema.newMessage(); 
		  byte[] array = new byte[in.readableBytes()];
		  in.readBytes(array); 
		  ProtobufIOUtil.mergeFrom(array, msg, schema);
		  out.add(msg);
		 
	}

}
