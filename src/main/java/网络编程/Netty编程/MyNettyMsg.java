package 网络编程.Netty编程;

import java.io.Serializable;

/**
 * @function 供netty发送对象消息使用
 * @author 肖荣辉
 * @date 2020年9月16日
*/
public class MyNettyMsg implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String content;
	

	public MyNettyMsg(String content) {
		super();
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
