package Java工具收集;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import net.sourceforge.pinyin4j.PinyinHelper;

public class PinyinUtils {
	
	/**
	 * @function 获取拼音首字母（大写），并且排除汉字中夹杂的其它符号
	 * @param str 文字字符串
	 * @author 肖荣辉
	 * @date 2021年5月19日
	*/
	public static String getPinYinHeadChar(String str) {
       
		String convert = "";
        
        for (int i = 0; i < str.length(); i++) {
        	
            char word = str.charAt(i);
            
            //先判断其是否是汉字
            if(!String.valueOf(word).matches("[\\u4E00-\\u9FA5]+"))
            	continue;
            	
            //提取汉字的首字母
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            
            convert += pinyinArray != null  && pinyinArray.length > 0 ? pinyinArray[0].charAt(0) : word;
            
        }
        
        return convert.toUpperCase();
    }
	
	/**
	 * @function 获取汉语拼音
	 * @param str 文字字符串
	 * @author 肖荣辉
	 * @date 2021年5月19日
	 */
	public static String getPinYin(String str) {
		
		String pinyin = "";
		
		for (int i = 0; i < str.length(); i++) {
			
			char word = str.charAt(i);
			
			//先判断其是否是汉字
			if(!String.valueOf(word).matches("[\\u4E00-\\u9FA5]+"))
				continue;
			
			//提取汉字的首字母
			String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
			
			System.out.println(word + "==>" + Arrays.asList(pinyinArray));
			
			pinyin += StringUtils.join(pinyinArray);
			
		}
		
		return pinyin;
	}
	
}
