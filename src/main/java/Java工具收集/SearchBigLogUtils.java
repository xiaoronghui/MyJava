package Java工具收集;

import java.nio.charset.Charset;
import java.util.List;

import cn.hutool.core.io.FileUtil;

public class SearchBigLogUtils {
	
	
	public static void main(String[] args) {
		
		String keyWords = "标记/取消受试者接口";
		
		String filePath = "D:/logback.info.2024-12-12.0.log";
		
		// 查找指定关键字的文本.找到后每200行打印一个文件
		findText(keyWords, filePath);		
		
	}

	/**
	 * @function 查找指定关键字的文本.找到后每200行打印一个文件
	 * @param keyWords
	 * @param filePath
	 * @author 肖荣辉
	 * @date 2024年12月12日
	*/
	private static void findText(String keyWords, String filePath) {
		
		List<String> lines = FileUtil.readLines(filePath, Charset.forName("utf-8"));

		StringBuilder sb = new StringBuilder();
		
		//已追加的文本行数
		int textRows = 0;
		int maxRows = 200;
		
		//是否匹配到文本
		boolean findFLag = false;
		
		int fileIndex = 1;
		
		//每找到一份, 打印后200行. 然后继续找
		for(int i = 0 ; i < lines.size() ; i ++) {
			
			String line = lines.get(i);
			
			if(line.indexOf(keyWords) != -1) {
				System.out.println("第"+(i + 1)+"行找到了关键字 : [" + keyWords + "]");
				findFLag = true;
			}
			
			if(findFLag) {
				sb.append(line);
				textRows ++;
			}
			
			if(textRows >= maxRows) {
				String savePath = "D:/find-keywords-"+fileIndex+".txt";
				FileUtil.writeString(sb.toString(), savePath , Charset.forName("utf-8"));
				System.out.println("输出后" + maxRows + "行到文件" + savePath);
				textRows = 0;
				findFLag = false;
				sb = new StringBuilder();
				fileIndex ++;
			}
			
		}
		
		
	}
	
	
}
