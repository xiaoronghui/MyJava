package Java工具收集;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class Artifact {
	
	//唯一编号
	private String id;
	
	//jar包群组
	private String groupId;
	
	//jar包ID
	private String artifactId;

	//jar包版本
	private String version;
	
	//冲突信息
	private String conflictInfo;
	
	//子jar包集合
	private List<Artifact> childArtifactList = new ArrayList<Artifact>();
	
	//是否是冲突jar包
	private boolean isConflict = false;

	public Artifact(String groupId, String artifactId,  String version, String conflictInfo,
			List<Artifact> childArtifactList, boolean isConflict) {
		this.id = UUID.randomUUID().toString();
		this.artifactId = artifactId;
		this.groupId = groupId;
		this.version = version;
		this.conflictInfo = conflictInfo;
		this.childArtifactList = childArtifactList;
		this.isConflict = isConflict;
	}

	public Artifact() {
		this.id = UUID.randomUUID().toString();
	}

	public Artifact( String groupId, String artifactId , String version, String conflictInfo) {
		this.id = UUID.randomUUID().toString();
		this.artifactId = artifactId;
		this.groupId = groupId;
		this.version = version;
		this.conflictInfo = conflictInfo;
	}

	public Artifact(String groupId, String artifactId,  String version, String conflictInfo, boolean isConflict) {
		this.id = UUID.randomUUID().toString();
		this.artifactId = artifactId;
		this.groupId = groupId;
		this.version = version;
		this.conflictInfo = conflictInfo;
		this.isConflict = isConflict;
	}
	
	
}
