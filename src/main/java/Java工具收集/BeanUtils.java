package Java工具收集;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;

public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {
	
	/**
	* @function 把源对象的属性值拷贝到目标对象的同名属性中,忽略null值属性
	* @param dest 目标对象
	* @param orig 源对象
	* @author 肖荣辉
	* @date 2018年7月9日
	*/
	public static void copyProperties(final Object dest , final Object orig) throws IllegalAccessException, InvocationTargetException{
		copyProperties(dest, orig, true);
	}
	
	
	/**
	* @function 把源对象的属性值拷贝到目标对象的同名属性中
	* @param dest 目标对象
	* @param orig 源对象
	* @param isIgnoreNullPropertys 是否忽略null值属性
	* @author 肖荣辉
	* @date 2018年7月9日
	*/
	public static void copyProperties(final Object dest , final Object orig , boolean isIgnoreNullPropertys) throws IllegalAccessException, InvocationTargetException{
		
		if(dest == null || dest == null)
			return;
		
		/*若不忽略null值属性,则直接拷贝*/
		if(!isIgnoreNullPropertys)
			org.apache.commons.beanutils.BeanUtils.copyProperties(dest, orig);
		
		Field[] fields = orig.getClass().getDeclaredFields();
		
		for(int i = 0 ; fields != null && i < fields.length ; i++){
			Field field = fields[i];
			field.setAccessible(true);
			Object value = field.get(orig);
			if(value != null)
				org.apache.commons.beanutils.BeanUtils.copyProperty(dest, field.getName(), value);
		}
		
	}
	
	/**
	 * @function 去除前后空格
	 * @author 肖荣辉
	 * @date 2021年3月24日
	*/
	public static <T> void  trim(T t ) {
		
		try {
			
			Field[] fields = t.getClass().getDeclaredFields();
			
			if(fields == null || fields.length == 0)
				return;
			
			for(Field item : fields) {
				
				item.setAccessible(true);
				
				//若不是字符串类型则忽略
				if(!item.getType().equals(String.class))
					continue;
				
				Object obj = item.get(t);
				
				//为null则忽略
				if(obj == null)
					continue;
				
				item.set(t , StringUtils.trim(obj.toString()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return;
	}
	
	
}
