package Java工具收集;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.http.webservice.SoapClient;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * @function webservice工具类
 * @author 肖荣辉
 * @date 2021年12月28日
*/
public class WebserviceUtils {
	
	/**
	 * @function 调用webservice接口 , 返回JSONObject
	 * @param url webservice接口地址
	 * @param namespaceURI 命名空间URI
	 * @param methodName 方法名称
	 * @param params 参数map
	 * @author 肖荣辉
	 * @date 2021年12月28日
	*/
	public static JSONObject callWebservice(String url ,  String namespaceURI , String methodName ,  Map<String, Object> params ) {
		
		//false 表示方法名可以不带前缀
		SoapClient soapClient = SoapClient.create(url).setMethod(methodName , namespaceURI).setParams(params , false);
		
		System.out.println("参数 : ");
		
		System.out.println(	soapClient.getMsgStr(true));
	
		//发送请求 , 返回xml不需要美化
		String xmRst = soapClient.send(true);
		
		System.out.println("xml的结果 :");
		
		System.out.println(xmRst);
		
		//xml转map形式
		JSONObject data = BeanUtil.toBean(XmlUtil.xmlToMap(xmRst) , JSONObject.class);
		
		System.out.println(JSONUtil.toJsonPrettyStr(data));
		
		return data;
		
	}
	
	public static void main(String[] args) throws Exception {
		
		
		//http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl
		
		
		/**先用soapUI解析http://127.0.0.1:3333/web_server/calculator?wsdl文件,会得到各个方法和参数之间的关系*/
//		Map<String, Object> params = new HashMap<>();
//		params.put("arg0", 1);
//		params.put("arg1", 2);
//		
//		callWebservice("http://127.0.0.1:3333/web_server/calculator", "http://WebServiceStudyServer.xrh.com/", "getPerson", params);
		
		Map<String, Object> params = new HashMap<>();
		params.put("byProvinceName", "浙江");
		
		callWebservice("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx", "http://WebXml.com.cn/", "getSupportCity", params);
		
	}
	
	
	
}