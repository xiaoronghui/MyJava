package Java工具收集;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class ArtifactVersionBean {
	
	//最高版本号的jar包
	private Artifact highestVersionArtifact; 
	
	//冲突的jar包集合
	private List<Artifact> conflictArtifactList;
	
	//需要排除冲突的jar包集合
	private List<Artifact> excludeConflictArtifactList;
	
	//冲突的版本号
	private Collection<String> conflictVersionList;
	
	//冲突的群组ID
	private String conflictGroupId;
	
	//冲突的jar包ID
	private String conflictArtifactId;
	
	//子节点和父节点对照map
	private Map<String , String> childIdParentIdMap;


	public ArtifactVersionBean() {
		super();
	}

	public ArtifactVersionBean(Artifact highestVersionArtifact, List<Artifact> conflictArtifactList,
			List<Artifact> excludeConflictArtifactList, Collection<String> conflictVersionList, String conflictGroupId,
			String conflictArtifactId, Map<String, String> childIdParentIdMap) {
		super();
		this.highestVersionArtifact = highestVersionArtifact;
		this.conflictArtifactList = conflictArtifactList;
		this.excludeConflictArtifactList = excludeConflictArtifactList;
		this.conflictVersionList = conflictVersionList;
		this.conflictGroupId = conflictGroupId;
		this.conflictArtifactId = conflictArtifactId;
		this.childIdParentIdMap = childIdParentIdMap;
	}
	
	
	
}
