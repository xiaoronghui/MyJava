package Java工具收集;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


/**
    Spring应用 4  ApplicationContextAware应用理解
	Aware接口的Bean在被初始之后，可以取得一些相对应的资源。
	Aware接口本身并不具备什么功能，一般是用于子类继承后，Spring上下文初始化bean的时候会对这个bean传入需要的资源。
	例如ApplicationContextAware接口，可以在Spring初始化实例 Bean的时候，可以通过这个接口将当前的Spring上下文传入。
	注意：一定要让继承ApplicationContextAware接口的bean被Spring上下文管理，在application.xml文件中定义对应的bean标签，或者使用@Component标注。
 */
@Component
public class SpringBeanUtils implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		SpringBeanUtils.applicationContext = applicationContext;
	}

	public static <T> T getBean(String beanName, Class<T> requiredType) {
		return applicationContext.getBean(beanName, requiredType);	
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName) {
		return (T) applicationContext.getBean(beanName);
	}
	
	public static <T> T getBean(Class<T> requiredType) {
		return applicationContext.getBean(requiredType);
	}
}
