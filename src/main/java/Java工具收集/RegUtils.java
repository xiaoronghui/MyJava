package Java工具收集;

import java.util.Calendar;
import java.util.Date;

public class RegUtils {

	
	public static void main(String[] args) {
		
		Date startDate =  DateUtils.parseDate("2021-10-13 00:00:00"); 
		
		Date endDate =  DateUtils.parseDate("2021-12-13 23:59:59"); 
		
		String reg = dateRangeToReg(startDate, endDate);
		
		System.out.println(reg);
		
	}
	
	
	/**
	 * @function 日期范围转化为正则表达式
	 * @param startDate 开始日期
	 * @param endDate 截止日期
	 * @author 肖荣辉
	 * @date 2021年10月13日
	*/
	public static String dateRangeToReg(Date startDate , Date endDate){
		Calendar   calendar   =   Calendar.getInstance(); 
		calendar.setTime(startDate);
		int startyear =calendar.get(Calendar.YEAR); 
		int startmonth=calendar.get(Calendar.MONTH)+1; 
		String startmonthStr=String.valueOf(startmonth);
		if(startmonth<10)startmonthStr="0"+startmonth;
		int startday=calendar.get(Calendar.DATE);
		calendar.setTime(endDate);
		int endyear =calendar.get(Calendar.YEAR); 
		int endmonth=calendar.get(Calendar.MONTH)+1; 
		String endmonthStr=String.valueOf(endmonth);
		if(endmonth<10)endmonthStr="0"+endmonth;
		int endday=calendar.get(Calendar.DATE); 
		StringBuffer sb=new StringBuffer();
		if(endyear-startyear>0){//跨年情况，获得月份正则表达式	
			if(startmonth<9){
				sb.append("\"");
				sb.append(startyear);
				sb.append("-0[");
				sb.append(startmonth+1);
				sb.append("-9]\",");
				sb.append("\"");
				sb.append(startyear);
				sb.append("-1[0-2]\",");
			}
			else if(startmonth!=12){
				sb.append("\"");
				sb.append(startyear);
				sb.append("-1[");
				sb.append(startmonth%10+1);
				sb.append("-2]\",");
			}
			if(1< endmonth&&endmonth<11){
				sb.append("\"");
				sb.append(endyear);
				sb.append("-0[1-");
				sb.append(endmonth-1);
				sb.append("]\",");
			}else if(endmonth!=1){
				sb.append("\"");
				sb.append(endyear);
				sb.append("-[0-9]");
				sb.append("\",\"");
				sb.append(endyear);
				sb.append("-1[0-");
				sb.append(endmonth%10-1);
				sb.append("]\",");
			}
			sb.append(differentMonthDayReg(startyear,startmonthStr,startday,endyear,endmonthStr,endday));
		}
		else{//不跨年情况，获得月份正则表达式
			if(endmonth-startmonth>1){//相隔了两个月以上
				if(endmonth<11){
					sb.append("\"");
					sb.append(startyear);
					sb.append("-");
				sb.append("0[");	
				sb.append(startmonth+1);
				sb.append("-");
				sb.append(endmonth-1);
				sb.append("]\",");
				}else if(startmonth>8){
					sb.append("\"");
					sb.append(startyear);
					sb.append("-");
					sb.append("1[");	
					sb.append((startmonth+1)%10);
					sb.append("-");
					sb.append(endmonth%10-1);
					sb.append("]\",");
				}
				else {
					sb.append("\"");
					sb.append(startyear);
					sb.append("-0[");
					sb.append(startmonth+1);
					sb.append("-9]\",\"");
						sb.append(startyear);
						sb.append("-1[0-");
						sb.append(endmonth%10-1);
						sb.append("]\",");	            
				}
				sb.append(differentMonthDayReg(startyear,startmonthStr,startday,endyear,endmonthStr,endday));
			}else if(endmonth-startmonth==1){//只隔了一个月情况
				sb.append(differentMonthDayReg(startyear,startmonthStr,startday,endyear,endmonthStr,endday));
			}else {//同一个月的情况
				if(endday/10-startday/10>1){
					sb.append("\"");
		        	sb.append(startyear); 
		        	sb.append("-"); 
		        	sb.append(startmonthStr); 
		        	sb.append("-[");
		        	sb.append(startday/10+1); 
		        	sb.append("-");
		        	sb.append(endday/10-1); 
		        	sb.append("]\",\"");
		        	sb.append(startyear);
		        	sb.append("-"); 
		        	sb.append(startmonthStr); 
		        	sb.append("-");
		        	sb.append(startday/10); 
		        	sb.append("[");
		        	sb.append(startday%10);
		        	sb.append("-9]\",\"");
		        	sb.append(endyear);
		        	sb.append("-"); 
		        	sb.append(endmonthStr); 
		        	sb.append("-");
		        	sb.append(endday/10); 
		        	sb.append("[0-");
		        	sb.append(endday%10);
		        	sb.append("]\"");
				}else if(endday/10-startday/10==1){
		        	sb.append("\"");
		        	sb.append(startyear);
		        	sb.append("-"); 
		        	sb.append(startmonthStr); 
		        	sb.append("-");
		        	sb.append(startday/10); 
		        	sb.append("[");
		        	sb.append(startday%10);
		        	sb.append("-9]\",\"");
		        	sb.append(endyear);
		        	sb.append("-"); 
		        	sb.append(endmonthStr); 
		        	sb.append("-");
		        	sb.append(endday/10); 
		        	sb.append("[0-");
		        	sb.append(endday%10);
		        	sb.append("]\"");
				}else{
					sb.append("\"");
		        	sb.append(startyear);
		        	sb.append("-"); 
		        	sb.append(startmonthStr); 
		        	sb.append("-");
		        	sb.append(startday/10); 
		        	sb.append("[");
		        	sb.append(startday%10);
		        	sb.append("-");
		        	sb.append(endday%10);
		        	sb.append("]\"");
				}
			}

		}

		return sb.toString();	
	}

	public  static String differentMonthDayReg(int startyear,String startmonthStr,int startday,int endyear,String endmonthStr,int endday){

		StringBuffer sb=new StringBuffer();
        if(startday/10<3){
        	sb.append("\"");
        	sb.append(startyear); 
        	sb.append("-"); 
        	sb.append(startmonthStr); 
        	sb.append("-[");
        	sb.append(startday/10+1); 
        	sb.append("-3]\",\"");
        	sb.append(startyear); 
        	sb.append("-"); 
        	sb.append(startmonthStr); 
        	sb.append("-");
        	sb.append(startday/10); 
        	sb.append("[");
        	sb.append(startday%10); 
        	sb.append("-9]\",");       	
        }
        else {
        	sb.append("\"");
        	sb.append(startyear); 
        	sb.append("-"); 
        	sb.append(startmonthStr); 
        	sb.append("-3[");
        	sb.append(startday%10); 
        	sb.append("-1]\",");
        }
        
        if(endday/10>0){
        	sb.append("\"");
        	sb.append(endyear); 
        	sb.append("-"); 
        	sb.append(endmonthStr); 
        	sb.append("-[0-");
        	sb.append(endday/10-1); 
        	sb.append("]\",");
        	sb.append("\"");
        	sb.append(endyear); 
        	sb.append("-"); 
        	sb.append(endmonthStr);
        	sb.append("-");
        	sb.append(endday/10); 
        	sb.append("[0-");
        	sb.append(endday%10); 
        	sb.append("]\"");
        }
        else {
        	sb.append("\"");
        	sb.append(endyear);
        	sb.append("-"); 
        	sb.append(endmonthStr);
        	sb.append("-");
        	sb.append(endday/10); 
        	sb.append("[0-");
        	sb.append(endday%10); 
        	sb.append("]\"");
        }
        
		return sb.toString();
	}

	
	
	
}
