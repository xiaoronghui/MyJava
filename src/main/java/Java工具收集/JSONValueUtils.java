package Java工具收集;

import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * @function 封装一个json获取值的工具类
 * @author 肖荣辉
 * @date 2023年2月10日
*/
public class JSONValueUtils {
	
	/**
	 *  获取msg中的行数据
	 *  @param data 饭后的数据
	 *  @param rowKeyPath 相对于Msg标签下每一行的记录的key路径, 举例：msg.body.row , 表示从data中获取Msg标签下的msg下body下的row的值
	 */
	public static List<JSONObject> getRowDataArray(JSONObject data , String rowKeyPath) {
		
		JSONObject MsgInfo = getJSONObjectFromJSONObject(data , "MsgInfo");
		
		List<JSONObject> rowDataList = new ArrayList<JSONObject>();
		
		if(MapUtil.isEmpty(MsgInfo))
			return rowDataList;
			
		try {
			
			JSONArray array = MsgInfo.getJSONArray("Msg");
			
			for(int i = 0 ; i < array.size() ; i ++) {
				
				String item = array.getStr(i);
				
				if(StrUtil.isBlank(item))
					continue;
				
				JSONObject msgItem = JSONUtil.isJson(item) ? JSONUtil.parseObj(item) : JSONUtil.xmlToJson(item);
				
				JSONObject rowData = getJSONObjectFromJSONObject(msgItem , rowKeyPath);
				
				rowDataList.add(rowData);
				
			}
			
			return rowDataList;
	
		} catch (Exception e) {
			
			String item = MsgInfo.getStr("Msg");
			
			if(StrUtil.isBlank(item))
				return rowDataList;
			
			JSONObject msgItem = JSONUtil.isJson(item) ? JSONUtil.parseObj(item) : JSONUtil.xmlToJson(item);
			
			//默认行数据就是MSG , 否则根据rowKeyPath从下面取值
			JSONObject rowData = StrUtil.isBlank(rowKeyPath) ? msgItem : getJSONObjectFromJSONObject(msgItem , rowKeyPath);
			
			rowDataList.add(rowData);
		}
		
		return rowDataList;
	}
	
	/**
	 * @function  获取某个路径上key的JSONObject值
	 * @param keyPath key路径 ， 举例：msg.body.row , 表示从data中获取msg下body下的row的值
	 * @author 肖荣辉
	 * @date 2022年12月9日
	 */
	public static JSONObject getJSONObjectFromJSONObject(JSONObject data ,  String keyPath) {
		return getValueFromJSONObject(data, JSONObject.class, keyPath);
	}
	
	
	/**
	 * @function  获取某个路径上key的String值
	 * @param keyPath key路径 ， 举例：msg.body.row , 表示从data中获取msg下body下的row的值
	 * @author 肖荣辉
	 * @date 2022年12月9日
	 */
	public static String getStrFromJSONObject(JSONObject data ,  String keyPath) {
		return getValueFromJSONObject(data, String.class, keyPath);
	}
	
	/**
	 * @function  获取某个路径上key的int值
	 * @param keyPath key路径 ， 举例：msg.body.row , 表示从data中获取msg下body下的row的值
	 * @author 肖荣辉
	 * @date 2022年12月9日
	 */
	public static Integer getIntFromJSONObject(JSONObject data ,  String keyPath) {
		return getValueFromJSONObject(data, Integer.class, keyPath);
	}
	
	/**
	 * @function  获取某个路径上key的值
	 * @param keyPath key路径 ， 举例：msg.body.row , 表示从data中获取msg下body下的row的值
	 * @author 肖荣辉
	 * @date 2022年12月9日
	 */
	public static <T> T getValueFromJSONObject(JSONObject data , Class<T> clazz , String keyPath) {
		
		if(MapUtil.isEmpty(data))
			return null;
		
		if(StrUtil.isBlank(keyPath))
			return null;
		
		String[] keys = keyPath.split("\\.");
		
		for(int i = 0 ; i< keys.length ; i ++) {
			
			String key = keys[i];
			
			if(MapUtil.isEmpty(data))
				return null;
			
			if(!data.containsKey(key))
				return null;
			
			//若不是最后一个
			if((i + 1) != keys.length) {
				data = data.getJSONObject(key);
				continue;
			}
			
			return data.get(key, clazz);
		}
		
		return null;
	}
	

}
