package Java工具收集;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * @function 获取mac地址工具类
 * 参考 :  https://jingyan.baidu.com/article/642c9d34ff9da7644a46f70c.html
 * @author 肖荣辉
 * @date 2020年9月11日
*/
public class MacUtils {
	
	public static void main(String[] args) throws SocketException {
		System.out.println(getMACAddr());
	}
	
	/**
	 * @function 获取当前机器上所有物理网卡的mac地址
	 * @author 肖荣辉
	 * @date 2020年9月11日
	*/
	public static Set<String> getMACAddr() throws SocketException  {
		
		/**
		 * 顾名思义，NetworkInterface 用于表示一个网络接口，这可以是一个物理的网络接口，也可以是一个虚拟的网络接口，
		 * 而一个网络接口通常由一个 IP 地址来表示。既然 NetworkInterface 用来表示一个网络接口，那么如果可以获得当前机器所有的网络接口
		 * （包括物理的和虚拟的），然后筛选出表示局域网的那个网络接口，那就可以得到机器在局域网内的 IP 地址。
		 * 
		 * 通过 API 文档可知，使用getNetworkInterfaces方法即可得到当前机器上所有的网络接口
		 */
		Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
		
	    Set<String> macList = new HashSet<>();
	    
	    //可能存在多块网卡
	    while (allNetInterfaces.hasMoreElements()) {
            
		  NetworkInterface netInterface = allNetInterfaces.nextElement();
          
		  /**
		   *   boolean isLoopback()返回网络接口是否是回送接口
				boolean isPointToPoint()返回网络接口是否是点对点接口
				boolean isUp()返回网络接口是否已经开启并运行
				boolean isVirtual()返回此接口是否是虚拟接口（也称为子接口）。
		   * 
		   */
		  if(netInterface.isLoopback() || netInterface.isVirtual() || netInterface.isPointToPoint() || !netInterface.isUp())
			  continue;
            
		  //byte[] getHardwareAddress() 获取网络接口的物理地址,通常是MAC地址 , 只取物理地址
		  byte[] mac = netInterface.getHardwareAddress();
		  
		  if(mac == null) continue;
		  
          StringBuilder sb = new StringBuilder();
          
          for (int i = 0; i < mac.length; i++) {
        	  //16进制 , 中间用冒号隔开 , mac地址是48位 ==>  xx : xx : xx : xx : xx : xx
              sb.append(String.format("%02x%s", mac[i] , (i < mac.length - 1) ? ":" : ""));
          }
          
          macList.add(sb.toString().toUpperCase());
          
        }
	    
		return macList;
	}
	
	
	public static void getMACAddr2() throws SocketException, UnknownHostException {

		NetworkInterface netInterface = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());

		byte[] mac = netInterface.getHardwareAddress();

		 StringBuilder sb = new StringBuilder();
         
         for (int i = 0; i < mac.length; i++) {
       	  //16进制 , 中间用冒号隔开 , mac地址是48位 ==>  xx : xx : xx : xx : xx : xx
             sb.append(String.format("%02x%s", mac[i] , (i < mac.length - 1) ? ":" : ""));
         }
         
         System.out.println(sb);
		}
	
	

}
