package Java工具收集;

import java.io.File;
import java.util.List;

import cn.hutool.core.io.FileUtil;

public class ModifyImportDataBaseSql {
	
	public static void main(String[] args) {
		
		File file  = new File("D:/gcp_all_data.sql");
		
	    List<String> list = FileUtil.readLines(file, "utf-8"); 
		
	    StringBuilder txt = new StringBuilder();
	    
	    boolean mysqlFlag = false;
	    
	    StringBuilder ignoreTxt = new StringBuilder();
	    
	    for(String s : list) {
	    	
	  
	    	
	    	if(s.indexOf("USE `mysql`") != -1) {
	    		mysqlFlag = true;
	    	}

	    	if(s.indexOf("USE `cro-management-platform`") != -1 ) {
	    		mysqlFlag = false;
	    	}
	    	
	    	if(s.indexOf("USE `nacosconfig`") != -1 ) {
	    		mysqlFlag = false;
	    	}
	    	
	      	if(s.indexOf("USE `") != -1 && !mysqlFlag) {
	      		
	      		String dbName = s.substring(s.indexOf("`") + 1 , s.lastIndexOf("`"));
	      		
	      		String createDbTxt = "CREATE DATABASE IF NOT EXISTS  `"+dbName+"`  CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_0900_ai_ci';";
	      		
	      		txt.append(createDbTxt);
	      		
	      		txt.append("\n");
	    	}
	    	
	    	if(!mysqlFlag) {
	    		txt.append("\n").append(s);
	    	}else {
	    		ignoreTxt.append(s);
//	    		System.out.println("忽略:\n" + s);
	    	}
	    	
	    }
	    
	    FileUtil.writeString(txt.toString(), new File("D:/gcp_all_data-new.sql"), "utf-8");
	    FileUtil.writeString(ignoreTxt.toString(), new File("D:/ignore.sql"), "utf-8");
		
	}
	
}
