package Java工具收集;

import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.poi.word.Word07Writer;

public class WordUtils {

	public static void main(String[] args) {
		
		 // 创建Word07Writer对象，用于生成Word文档
        Word07Writer writer = new Word07Writer();
        
        Font font = new Font("宋体", Font.BOLD, 16);
 
        // 添加一个段落
        writer.addText(font , "病房数据库设计：");
        
        List<List<String>> data = new ArrayList<List<String>>();
        
        data.add(CollUtil.newArrayList("姓名" , "年龄" , "爱好"));
        data.add(CollUtil.newArrayList("张三" , "34" , "篮球"));
        data.add(CollUtil.newArrayList("李四" , "25" , "足球"));
        
        // 添加表格
        writer.addTable(data);;
 
        writer.flush(new File("D:/导出gcp_ward.docx"));
 
        // 关闭writer
        writer.close();
		
	}
	
	
	
}
