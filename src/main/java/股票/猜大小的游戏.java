package 股票;

import java.util.Random;


/**
 *  凯利公式 :  f = p(b+1)-1/b
 * 
 * b = 赔率          ==>    1
 * p = 赢钱的几率  ==> 50%
 * f = 下注的资金比例 
 * 
 *  0.5(1 + 1) - 1 / 1 = 0%
 *  
 *  随机猜大小的游戏 , 因为资金不是无限的 , 只要命中一次就会血本无归 , 时间越长 , 亏损几率越大
 *  
 *  结论 :  只要不符合凯利公式的  , 统统都会长赌必输
 *
 */
public class 猜大小的游戏 {
	
	static Random random = new Random();
	
	public static void main(String[] args) {
		
		double total = 81910;
		
		int cs = (30 * 24 * 60) / 5;
		
		System.out.println("一天 : " + cs + "次");
		
		int years = 12;
		int winCount = 0;
		int loseCount = 0;
		double winTotal = 0;
		double loseTotal = 0;
		
		for(int i = 0 ; i < years ; i ++) {
			double balance = tz(total , cs);
			
			double win = balance - total;
			
			if(win > 0) {
				winCount ++;
				winTotal += win;
			}else {
				loseCount ++;
				loseTotal += win;
//				System.out.println("you lose ! " +  i  + " balance  : " + balance   + " winTotal : " +  winTotal);
//				break;
			}
			
			System.out.println("win : " + (win));
		}
		
		System.out.println("总计 :  " + (winTotal + loseTotal ));
		System.out.println("胜率 : " + ((double)winCount/years));
		System.out.println("输率 : " + ((double)loseCount/years));
		
		//胜率 : 0.44
		//胜率 : 0.56
		
	}
	
	
	public static double tz(double total , int cs) {
		
		int tz = 1;
		double balance = total;
		
		double maxTz = 0;
		double maxBalance = 0;
		
		for(int i = 0 ; i < cs ; i ++) {
			
			int ya = random();
			
			int ran = random();
			
			boolean flag = false;
			
			if(balance <= tz) {
//				System.out.println("you lose all money !! current balance : " + balance  + " , you play "+i+" times , bet : " + tz);
				break;
			}
			
			//win
			if(ya == ran) {
				balance+=tz;
				tz = 1;
				flag = true;
			}
			//lose
			else {
				balance-= tz;
				tz  *= 2;
			}
			
			maxTz = maxTz >=  tz ? maxTz : tz;
			maxBalance = maxBalance >=  balance ? maxBalance : balance;
			
//			System.out.println("you bet  : " + tz  + "  , balance : "   + balance + (flag ? " win " : " lose"));
			
//			if(balance <= 0) {
//				System.out.println("you lose all money !! current balance : " + balance  + " , you play "+i+" times");
//				break;
//			}
			
		}
		
//		System.out.println("max : " + maxTz + " maxBalance : " + maxBalance );
		
		return balance;
	}
	
	
	public static  int  random() {
		return random.nextInt(2);
	}
	
	
	public static void test(long min , long max , int times) {
		
		
	}
	
	
}
