package 股票;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Week;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * @function 双色球
 * @author 肖荣辉
 * @date 2022年10月12日
*/
public class DoubleColorBall {
	
	//红球
	public final static List<String>  RED_BALLS = CollUtil.newArrayList("01" , "02" , "03" , "04" , "05" , "06" , "07" , "08" , "09" , "10" , "11" , "12" , "13" , "14" , "15" , "16" , "17" , "18" , "19" , "20" , "21" , "22" , "23" , "24" , "25" , "26" , "27" , "28" , "29" , "30" , "31" , "32" , "33");

	//篮球
	public final static  List<String> BLUE_BALLS = CollUtil.newArrayList("01" , "02" , "03" , "04" , "05" , "06" , "07" , "08" , "09" , "10" , "11" , "12" , "13" , "14" , "15" , "16");
	
	@SuppressWarnings("serial")
	public final static Map<Integer, Integer> levelMoneyMap = new HashMap<Integer , Integer>() {{
		this.put(1 , 5000000);
		this.put(2 , 200000);
		this.put(3 , 300);
		this.put(4 , 200);
		this.put(5 , 10);
		this.put(6 , 5);
	}};

	public static void main(String[] args) {
		
		DoubleBall d1 = randomDoubleBall();
		
		DoubleBall d2 = randomDoubleBall();
		
		isWin(d1 , d2);
	}
	
	/**
	 * @function 计算是几等奖(0是没中奖)
	 * @param hitRedBallCount 命中红球个数
	 * @param hitBlueBallCount 命中篮球个数
	 * @author 肖荣辉
	 * @date 2022年10月17日
	*/
	public static int winLevel(int hitRedBallCount , int hitBlueBallCount ) {
		
		String str = hitRedBallCount + "+"  + hitBlueBallCount;
		
		//六等奖
		if("0+1".equals(str)  || "1+1".equals(str) || "2+1".equals(str))
			return 6;
		
		//五等奖
		if("3+1".equals(str)  || "4+0".equals(str))
			return 5;
		
		//四等奖
		if("5+0".equals(str)  || "4+1".equals(str))
			return 4;
		
		//三等奖
		if("5+1".equals(str))
			return 3;
		
		//二等奖
		if("6+0".equals(str))
			return 2;
		
		//一等奖
		if("6+1".equals(str))
			return 1;
		
		return 0;
	}
	
	
	/**
	 * @function 是否中奖
	 * @author 肖荣辉
	 * @date 2022年10月17日
	*/
	public static void isWin(DoubleBall yourBall , DoubleBall openDoubleBall) {
		
		System.out.println("您的号码 : " + yourBall);
		
		System.out.println("开奖号码 : " + openDoubleBall);
		
		//红球取交集
		Collection<String> intersection = CollUtil.intersection(yourBall.getRedBalls() , openDoubleBall.getRedBalls());
		
		//几等奖
		int level = winLevel(intersection.size() , yourBall.getBlueBall().equals(openDoubleBall.getBlueBall()) ? 1 : 0);
		
		if(level == 0) {
			System.out.println("很遗憾 , 你未中奖 !");
			return;
		}
		
		System.out.println("恭喜 , 您中了" + level + "等奖 ! 锣鼓喧天 , 鞭炮齐鸣 !");
		
	}

	/**
	 * @function 根据期数获取双色球开奖号码
	 * @param expect
	 * @author 肖荣辉
	 * @date 2022年10月17日
	 *  彩票种类标识，目前提供七种彩种， ssq：双色球，qlc：七乐彩，fc3d：福彩3D，cjdlt：超级大乐透，qxc：七星彩，pl3：排列3，pl5：排列(5)
	*/
	public static DoubleBall getDoubleBallOpenCode(String expect) {
		
		String url = "https://www.mxnzp.com/api/lottery/common/aim_lottery?expect="+expect+"&code=ssq&app_id=nglitcockqkhygil&app_secret=Tkl4WU5OcDR0cldqS0VjU3RKVUR3QT09";

		String json = HttpUtil.get(url);
		
		JSONObject obj = 	JSONUtil.parseObj(json);

		String openCode = obj.getJSONObject("data").getStr("openCode"); 
		
		String[] arr = openCode.split("\\+");
		
		String redBallStr = arr[0];
		
		String blueBallStr = arr[1];
		
		DoubleBall doubleBall = new DoubleBall();
		
		doubleBall.setRedBalls(CollUtil.newArrayList(redBallStr));
		
		doubleBall.setBlueBall(blueBallStr);
		
		return doubleBall;
		
	}
	
	/**
	 * @function 随机一个双色球
	 * @author 肖荣辉
	 * @date 2022年10月17日
	*/
	public static DoubleBall randomDoubleBall() {
		
		DoubleBall doubleBall = new DoubleBall();
		
		doubleBall.setRedBalls(RandomUtil.randomEleList(RED_BALLS , 6));
		
		doubleBall.setBlueBall(RandomUtil.randomEle(BLUE_BALLS));
		
		return doubleBall;
		
	}

	/**
	 * @function 打印购买信息
	 * @author 肖荣辉
	 * @date 2022年10月12日
	*/
	public static void printBuyInfo() {
		
		Date now = new Date();
		
		Week week = DateUtil.dayOfWeekEnum(now);
		
		String time = DateUtil.formatDateTime(now);
		
		if(Week.MONDAY.equals(week) || Week.TUESDAY.equals(week)) {
			System.out.println("老板，下单本周二期16注彩票，站点 【33017383】，当前时间【"+time+"】，号码如下 :");
		}else if(Week.WEDNESDAY.equals(week) || Week.THURSDAY.equals(week)){
			System.out.println("老板，下单本周四期16注彩票，站点 【33017383】，当前时间【"+time+"】，号码如下 :");
		}else if(Week.FRIDAY.equals(week) || Week.SATURDAY.equals(week) || Week.SUNDAY.equals(week)){
			System.out.println("老板，下单本周日期16注彩票，站点 【33017383】，当前时间【"+time+"】，号码如下 :");
		}
		
		//创建随机红球 , 并包含所有篮球的16注号码
		String number16 = createRandomRedBallsAllBlueBallsNumber();
		
		while(number16 == null) {
			number16 = createRandomRedBallsAllBlueBallsNumber();
		}
		
		System.out.println(number16);
	}
	
	
	/**
	 * @function 创建随机红球(包含红球所有号码) , 并包含所有篮球的16注号码
	 * @author 肖荣辉
	 * @date 2022年10月12日
	*/
	public static String createRandomRedBallsAllBlueBallsNumber() {
		
		//所有红球号码
		Set<String> set = new HashSet<String>();
		
		String number16 = "--------------------------------\n";
		
		for(int i = 0 ; i < 16;  i ++) {
			
			List<String> sixRedBalls = randomSixRedBall();
			
			set.addAll(sixRedBalls);
			
			String doubleBallNumber = StrUtil.join("  " , sixRedBalls) +  " + " +  BLUE_BALLS.get(i);
			
			number16 += doubleBallNumber +"\n";
			
			if(i > 0 && (i + 1) %5 == 0) {
				number16 += "--------------------------------\n";
			}
				
			
		}
		
		//计算
		Collection<String> c = CollUtil.subtract(RED_BALLS , set);
		
		return c.size() >0 ? null : number16;
	}
	
	
	/**
	 * @function 随机6个不重复的红球号码
	 * @author 肖荣辉
	 * @date 2022年10月12日
	*/
	public static List<String> randomSixRedBall() {
		return RandomUtil.randomEleList(RED_BALLS , 6);
	}
	
}
