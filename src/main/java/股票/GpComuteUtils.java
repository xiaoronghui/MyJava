package 股票;

//股票计算工具类
public class GpComuteUtils {
	
	//每次只赚取10% , 我需要做对17次即可 .  每次的时间可能需要我2个月左右 ,  那么就是34个月. 一年半的时间.
	public static void main(String[] args) {
		
		//本金多少钱 
		double capital = 200000;
		
		//期望赚多少钱
		double expect = 1000000;
		
		//每次做对期待的涨幅
		double gain = 0.1;
		
		computeDays(capital, expect, gain);
	}
	
	/**
	 * 
	 * 设置本金 ,  期望赚到的钱 , 每天需求的涨幅 , 需要做对多少次
	 * @param capital 本金
	 * @param expect 期望赚多少钱
	 * @param gain 涨幅
	 * @return 返回需要的天数
	 */
	public static long computeDays(double capital , double expect , double gain ){
		
		//公式 :    capital * (1 + gain) ^ x  = capital + expect ==> 求x的值是多少
		
		//目标值
		double mb = capital + expect;
		
		//对数 
		double N = mb/capital;
		
		//底数
		double a =  1 + gain;
		
		/*Java中任意对数函数求法 : 
			在java中求logxN，首先要弄明白一个初中学到的公式logxN=logeN/logex,
			logeN代表以e为底的N的对数 , logex代表以e为底的x的对数.  
			在java.lang.math类中的log(double   a)代表以e为底的a的对数,
			因此logxN在Java中的表示为:   
            log((double)N)/log((double)x) */
            
		//次数
		long  times = Math.round(Math.log(N)/Math.log(a));
		
		System.out.println("本金 : "+capital+" ,  如果每天能盈利"+Math.round(gain * 100)+"% , 期望赚  : "+expect+", 需要做对"+times+"次" );
		
		return times;
	}

}
