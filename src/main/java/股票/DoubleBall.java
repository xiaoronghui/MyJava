package 股票;

import java.util.List;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

@Data
public class DoubleBall {

	//红球
	private List<String> redBalls;
	
	//篮球
	private String blueBall;

	@Override
	public String toString() {
		return StrUtil.join("  " , redBalls) + " + " + blueBall;
	}
	
}
